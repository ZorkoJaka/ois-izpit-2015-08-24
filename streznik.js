var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  
  

  
  var maxID=0;
  for(var i in noviceSpomin){
    if (noviceSpomin[i].id>maxID){
      maxID=noviceSpomin[i].id;
    }
  }
  
  var stnovic=noviceSpomin.length;

	var q= req.query;
	var id=(maxID+1);
	var naslov = q.naslov;
	var povzetek = q.povzetek;
	var kategorija = q.kategorija;
	var postnaStevilka = q.postnaStevilka;
	var kraj = q.kraj;
	var povezava = q.povezava;
  
  
  if((naslov.length < 1) || (povzetek.length < 1) || (kategorija.length < 1) || (postnaStevilka.length < 1) || (povezava.length < 1) ){
    res.send("Napaka pri dodajanju novice!<br/><a href='javascript:window.history.back()'>Nazaj</a>");
    
  }else{
  
  
	var newinfo={ id: id , naslov: naslov, povzetek: povzetek, kategorija: kategorija , postnaStevilka: postnaStevilka , kraj: kraj , povezava: povezava}; 
	
	
	
	noviceSpomin[stnovic]=newinfo;
		
	res.redirect('/');
  }
  
});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
	var id=req.query.id;
	
	var indeks=-1;
	
	if(typeof id ==='undefined'){
		res.send("Napačna zahteva!<br/><a href='javascript:window.history.back()'>Nazaj</a>");
		
	}else{
	for(var i in noviceSpomin){
		if(noviceSpomin[i].id==id){
			indeks=i;
		}
	}
	if(indeks==-1){
		res.send("Novica z id-jem "+id+"ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
		
	}else{
		noviceSpomin.splice(indeks, 1);
		res.redirect('back');
	}
	}
	
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
